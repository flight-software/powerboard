#ifndef POWERBOARD_CALIB_H
#define POWERBOARD_CALIB_H

#include "powerboard.h"

// Some functions for things, inheriently nonlinear
int powerboard_calib_volt_to_watt_hour(double *pack_watt_hour, double *watt_hour);

#endif
