#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include "libtty.h"
#include "libdebug.h"
#include "powerboard_pib.h"

static int powerboard_pib_verify(const powerboard_pib_t* pib)
{
    fd_set rset;
    FD_ZERO(&rset);
    FD_SET(pib->fd, &rset);

    struct timeval tv = {.tv_sec = 1, .tv_usec = 0};

    switch(select(pib->fd + 1, &rset, NULL, NULL, &tv)) {
    case -1:;
        P_ERR("select failed, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    case 1:;
        uint8_t read_val;
        switch(read(pib->fd, &read_val, 1)) {
        case -1:;
            P_ERR("read failed, errno: %d (%s)", errno, strerror(errno));
            return EXIT_FAILURE;
        case 0:;
            P_ERR_STR("pib returned 0 bytes");
            return EXIT_FAILURE;
        default:;
            uint8_t expected = (uint8_t)(pib->mask | UINT8_C(0x80));
            if(read_val != expected) {
                P_ERR("Our pib mask prediction was wrong: 0x%x vs 0x%x", expected, read_val);
                return EXIT_FAILURE;
            }
        }
        return EXIT_SUCCESS;
    default:;
        P_ERR_STR("select not ready");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

int powerboard_pib_init(powerboard_pib_t* pib, const char* serial, speed_t speed)
{
    if(!pib) {
        P_ERR_STR("pib was NULL");
        return EXIT_FAILURE;
    }

    if(!serial) {
        P_ERR_STR("serial was NULL");
        return EXIT_FAILURE;
    }

    if(start_serial(&pib->fd, serial, speed, 1, 1)) {
        P_ERR_STR("start serial failed");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

void powerboard_pib_close(powerboard_pib_t* pib)
{
    close_serial(pib->fd);
    pib->fd = -1;
}

int powerboard_pib_enable(powerboard_pib_t* pib, uint8_t mask)
{
    pib->mask |= mask;
    return powerboard_pib_set(pib, pib->mask);
}

int powerboard_pib_disable(powerboard_pib_t* pib, uint8_t mask)
{
    pib->mask &= (uint8_t)(~mask);
    return powerboard_pib_set(pib, pib->mask);
}

int powerboard_pib_set(powerboard_pib_t* pib, uint8_t mask)
{
    pib->mask = mask;

    if(acquire_serial_lock(pib->fd)) {
        P_ERR_STR("failed to get the lock");
        return EXIT_FAILURE;
    }

    for(int i = 0; i < 5; i++) {
        P_ERR("PIB attempt %d/4", i);

        if(write(pib->fd, &(pib->mask), 1) != 1) {
            P_ERR("Cannot write hotswap status to PIB, errno: %d (%s)", errno, strerror(errno));
            continue;
        }

        if(powerboard_pib_verify(pib)) {
            P_ERR_STR("Cannot verify PIB");
            continue;
        }

        release_serial_lock(pib->fd);
        return EXIT_SUCCESS;
    }

    release_serial_lock(pib->fd);
    return EXIT_FAILURE;
}
