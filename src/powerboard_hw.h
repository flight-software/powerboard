#ifndef POWERBOARD_HW_H
#define POWERBOARD_HW_H

#include "liblog.h"

// All RPC just returns the latest value we have logged

// Everthing ran here is exported as liblog

// TODO: more granular update functions can give us more
// flexibility with refresh rates
int powerboard_update(void);

// There is one for each RPC, but not everything here has to
// be logged to a file (perc can be derived from volt, etc)
// extern'd so that PBd.c can use them in rpc functions

extern log_fs pack_volt;
extern log_fs pack_perc;
extern log_fs pack_temp;
extern log_fs pack_curr;

extern log_fs hs_curr;
extern log_fs hs_flt;

extern log_fs avg_perc;

int powerboard_init(void);
int powerboard_loop(void);
int powerboard_close(void);

int powerboard_hs_en(uint64_t);
int powerboard_hs_dis(uint64_t);

/*
int powerboard_htr_en(uint64_t);
int powerboard_htr_dis(uint64_t);
*/

#endif
