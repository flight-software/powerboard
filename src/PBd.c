#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>

#include "libdebug.h"
#include "librpc.h"
#include "libdaemon.h"
#include "powerboard_hw.h"
#include "powerboard.h"

struct pbd_rpc_elem_t
{
    void* ptr;
    size_t len;
};

typedef struct pbd_rpc_elem_t pbd_rpc_elem_t;

static rpc_server_fs rpc_server;

#ifdef SPAWN_HMD
static int tried_restarting_hmd = 0;
#endif

static int pbd_rpc_func(const void* in, size_t in_len, void* out, size_t* out_len, void* param)
{
    (void)in; (void)in_len;

    pbd_rpc_elem_t* rpc_elem = param;

    if(rpc_elem->len > *out_len) {
        P_ERR("rpc elem length is too big %zu vs %zu", rpc_elem->len, *out_len);
        return EXIT_FAILURE;
    }

    log_fs* log_ = rpc_elem->ptr;

    void* data = dll_peek_head(&log_->list);

    if(!data) {
        P_ERR_STR("We have no data to report");
        *out_len = 0;
        return EXIT_FAILURE;
    }

    memcpy(out, data, rpc_elem->len);

    *out_len = rpc_elem->len;

    return EXIT_SUCCESS;
}

static int pbd_rpc_bitmask(const void* in, size_t in_len, void* out, size_t* out_len, void* func)
{
    (void)out;
    *out_len = 0;

    // 16 bits for hotswaps and another for PIB
    if(in_len != sizeof(uint32_t)) {
        P_ERR("in len %zu bytes must be %zu bytes", in_len, sizeof(uint32_t));
        return EXIT_FAILURE;
    }

    uint32_t real_in = 0;
    memcpy(&real_in, in, sizeof(uint32_t));

    P_INFO("rpc bitmask 0x%"PRIx32, real_in);

    int (*cb)(uint64_t);

    CAST_TO_FUNC_PTR(cb, func);

    if(cb(real_in)) {
        P_ERR_STR("callback failed");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

static pbd_rpc_elem_t rpc_pack_volt = { .ptr = &pack_volt, .len = sizeof(double) * PBD_LEN_PACK };
static pbd_rpc_elem_t rpc_pack_temp = { .ptr = &pack_temp, .len = sizeof(float)  * PBD_LEN_PACK };
static pbd_rpc_elem_t rpc_pack_perc = { .ptr = &pack_perc, .len = sizeof(double) * PBD_LEN_PACK };
static pbd_rpc_elem_t rpc_avg_perc  = { .ptr = &avg_perc,  .len = sizeof(double)                };
static pbd_rpc_elem_t rpc_hs_curr   = { .ptr = &hs_curr,   .len = sizeof(double) * PBD_LEN_HS   };
static pbd_rpc_elem_t rpc_hs_flt    = { .ptr = &hs_flt,    .len = sizeof(uint64_t)              };

static log_rpc_shim_fs log_shim = {
    .logs = (log_fs*[7]) {
        &pack_volt,
        &pack_perc,
        &pack_temp,
        &pack_curr,
        &hs_curr,
        &hs_flt,
        &avg_perc,
    },
    .num_logs = 7
};

static int pbd_rpc_init(void)
{
    // NOTE: This isn't really memory safe, but this won't be the first thing
    // to break, so we're gucci
    rpc_server_init(&rpc_server, PBD_RPC_SOCKET, RPC_SERVER_MT);

    void* pbd_hs_en;
    void* pbd_hs_dis;

    CAST_FUNC_TO_VOID_PTR(pbd_hs_en, powerboard_hs_en);
    CAST_FUNC_TO_VOID_PTR(pbd_hs_dis, powerboard_hs_dis);

    rpc_server_add_elem(&rpc_server, PBD_OP_GET_PACK_VOLT,  pbd_rpc_func,    &rpc_pack_volt,    NULL);
    rpc_server_add_elem(&rpc_server, PBD_OP_GET_PACK_TEMP,  pbd_rpc_func,    &rpc_pack_temp,    NULL);
    rpc_server_add_elem(&rpc_server, PBD_OP_GET_PACK_PERC,  pbd_rpc_func,    &rpc_pack_perc,    NULL);
    rpc_server_add_elem(&rpc_server, PBD_OP_GET_AVG_PERC,   pbd_rpc_func,    &rpc_avg_perc,     NULL);
    rpc_server_add_elem(&rpc_server, PBD_OP_GET_HS_CURR,    pbd_rpc_func,    &rpc_hs_curr,      NULL);
    rpc_server_add_elem(&rpc_server, PBD_OP_GET_HS_FLT,     pbd_rpc_func,    &rpc_hs_flt,       NULL);
    rpc_server_add_elem(&rpc_server, PBD_OP_HS_EN,          pbd_rpc_bitmask, pbd_hs_en,  NULL);
    rpc_server_add_elem(&rpc_server, PBD_OP_HS_DIS,         pbd_rpc_bitmask, pbd_hs_dis, NULL);
    rpc_server_add_elem(&rpc_server, RPC_OP_SPLIT_LOG,      log_rpc_shim,    &log_shim,         NULL);
    //rpc_server_add_elem(&rpc_server, PBD_OP_HTR_EN,         pbd_rpc_bitmask, powerboard_htr_en, NULL);
    //rpc_server_add_elem(&rpc_server, PBD_OP_HTR_DIS,        pbd_rpc_bitmask, powerboard_htr_dis, NULL);

    return EXIT_SUCCESS;
}

int main(void)
{
#ifdef DAEMONIZE_PBD
    daemonize();
#endif
    P_INFO_STR("Starting PBd");
    if(pbd_rpc_init() || powerboard_init()) return EXIT_FAILURE;
    P_INFO_STR("Started PBd");

#ifdef SPAWN_HMD
    // I'm not using the HMD_BIN macor because this way we avoid that dependency
    // and so pb-d is super independent in function
    if(is_daemon_alive("hm-d")) {
        P_ERR("%s died, let's restart that...", "hm-d");
        if(startup_daemon("hm-d")) {
            P_ERR("Failed to restart %s, try again soon", "hm-d");
        }
    }
#endif

    while(1) {
        powerboard_loop(); // >= 13 seconds to execute (cause of sleeps)
#ifdef SPAWN_HMD
        if(is_daemon_alive("hm-d")) {
            P_ERR("%s died, let's restart that...", "hm-d");
            if(tried_restarting_hmd == 0) {
                if(startup_daemon("hm-d")) {
                    P_ERR("Failed to restart %s, try again soon", "hm-d");
                }
                tried_restarting_hmd = 1;
            } else {
                P_ERR_STR("Failed to start hm-d twice, rebooting");
                system("reboot");
                return EXIT_FAILURE;
            }
        }
#endif
    }

    return EXIT_SUCCESS;
}
