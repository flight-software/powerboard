#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <inttypes.h>
#include <math.h>

#include "libdebug.h"
#include "liblog.h"
#include "libtty.h"
#include "powerboard_hw.h"
#include "powerboard_calib.h"
#include "powerboard_pib.h"

// New calib data (packs are linear)
static const long double pbd_pack_volt_calib[PBD_LEN_PACK][2] = {
    {3.3L*2.0L/4096.0L, 0.0L},
    {3.3L*2.0L/4096.0L, 0.0L},
    {3.3L*2.0L/4096.0L, 0.0L},
    {3.3L*2.0L/4096.0L, 0.0L}
};

static const long double pbd_pack_curr_calib[1][2] = {{2.5L/1000000.0L, 0.005}};

static const long double pbd_hs_curr_calib[PBD_LEN_HS][2] = {
    {0.3987341772L, 0.0},
    {0.6L, 0.0},
    {0.3987341772L, 0.0},
    {0.3987341772L, 0.0},
    {0.3987341772L, 0.0},
    {0.3987341772L, 0.0},
    {1.20458891L, 0.0},
    {0.8005082592L, 0.0},
    {0.3987341772L, 0.0},
    {0.6L, 0.0},
    {0.8005082592L, 0.0},
    {0.6L, 0.0},
    {0.3987341772L, 0.0},
    {0.8005082592L, 0.0},
};

static int serial_fd;

log_fs pack_volt;
log_fs pack_perc;
log_fs pack_temp;
log_fs pack_curr;

log_fs hs_curr;
log_fs hs_flt;

log_fs avg_perc;

static uint64_t log_time;
static uint64_t log_count;

static int powerboard_send_byte(int fd, uint8_t byte);
static int powerboard_read_bytes(int fd, uint8_t* resp, ssize_t* resp_len);
static int powerboard_send_read_bytes(int fd, uint8_t byte, uint8_t* resp, ssize_t* resp_len);

static int powerboard_send_byte(int fd, uint8_t byte)
{
    uint8_t output[3] = {0xF0, 0x00, byte};
    ssize_t r = write(fd, output, 3);
    if(r != 3) {
        P_ERR("Failed to send byte 0x%x to powerboard, returned %zd, errno: %d (%s)", byte, r, errno, strerror(errno));
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

static int powerboard_read_bytes(int fd, uint8_t* resp, ssize_t* resp_len)
{
    {
        uint8_t start_byte = 0;
        if(read(fd, &start_byte, 1) != 1) {
            P_ERR("Failed to read start byte, errno: %d (%s)", errno, strerror(errno));
            *resp_len = 0;
            return EXIT_FAILURE;
        }
        if(start_byte != 0xFB) {
            P_ERR("Start byte was: %x", start_byte);
            *resp_len = 0;
            return EXIT_FAILURE;
        }
    }
    {
        size_t so_far = 0;
        do {
            ssize_t r = read(fd, resp + so_far, (size_t)(*resp_len));
            if(r < 0) {
                P_ERR("Cannot read response back, errno: %d (%s)", errno, strerror(errno));
                *resp_len = (ssize_t)so_far;
                return EXIT_FAILURE;
            }
            so_far += r;
            P_INFO("Only read %zu/%zu\n", so_far, (size_t)(*resp_len));
        }
        while(so_far < (size_t)(*resp_len));
        *resp_len = (ssize_t)so_far;
    }
    {
        uint8_t end_byte = 0;
        if(read(fd, &end_byte, 1) != 1) {
            P_ERR("Failed to read end byte, errno: %d (%s)", errno, strerror(errno));
            return EXIT_FAILURE;
        }
        if(end_byte != 0xFE) {
            P_ERR("End byte was: %x", end_byte);
            return EXIT_FAILURE;
        }
    }
    return EXIT_SUCCESS;
}

static int powerboard_send_read_bytes(int fd, uint8_t byte, uint8_t* resp, ssize_t* resp_len)
{
    if(powerboard_send_byte(fd, byte) == EXIT_SUCCESS) {
        return powerboard_read_bytes(fd, resp, resp_len);
    }
    return EXIT_FAILURE;
}

static int powerboard_update_pack_volt(void)
{
    uint8_t resp[8] = {0};
    ssize_t resp_len = sizeof(resp)/sizeof(resp[0]);
    
    if(acquire_serial_lock(serial_fd)) {
        P_ERR("Failed to acquire serial lock, errno: %d, (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    
    if(powerboard_send_read_bytes(serial_fd, 0x05, resp, &resp_len) == EXIT_FAILURE) {
        release_serial_lock(serial_fd);
        return EXIT_FAILURE;
    }
    
    if(release_serial_lock(serial_fd)) {
        P_ERR("Failed to release serial lock, errno: %d, (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    double last_pack_volt[PBD_LEN_PACK];
    uint16_t* adc = (uint16_t*)resp;
    for(int i = 0; i < PBD_LEN_PACK; i++) {
        adc[i] = be16toh(adc[i]);
        last_pack_volt[i] = (double)(pbd_pack_volt_calib[i][0] * (long double)adc[i] + pbd_pack_volt_calib[i][1]);
    }

    log_add(&pack_volt, last_pack_volt);
    return EXIT_SUCCESS;
}

static int powerboard_update_pack_temp(void)
{
    uint8_t resp[PBD_LEN_PACK * sizeof(uint16_t)] = {0};
    ssize_t resp_len = sizeof(resp) / sizeof(resp[0]);

    if(acquire_serial_lock(serial_fd)) {
        P_ERR("Failed to acquire serial lock, errno: %d, (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    
    if(powerboard_send_read_bytes(serial_fd, 0x06, (uint8_t*)resp, &resp_len) == EXIT_FAILURE) {
        release_serial_lock(serial_fd);
        return EXIT_FAILURE;
    }
    
    if(release_serial_lock(serial_fd)) {
        P_ERR("Failed to release serial lock, errno: %d, (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    if(resp_len != (ssize_t)(sizeof(resp) / sizeof(resp[0]))) {
        P_ERR("Temperatures returned %zd bytes", resp_len);
        return EXIT_FAILURE;
    }

    static const long double BETA = 4261.0L;

    uint16_t* adc = (uint16_t*)resp;
    long double interim[PBD_LEN_PACK];
    for(int i = 0; i < PBD_LEN_PACK; i++) {
        adc[i] = be16toh(adc[i]);
        interim[i] = (3.3L / ((long double)adc[i] * 3.3L/4096.0L)) * 100000.0L - 100000.0L;
    }

    float temps[PBD_LEN_PACK];
    for(int i = 0; i < PBD_LEN_PACK; i++) {
        long double numerator = BETA * (25.0L + 273.15L);
        long double denominator = BETA + logl(interim[i] / 50000.0L) * (25.0L + 273.15L) - 273.15L;
        temps[i] = (float)(numerator / denominator);
    }

    log_add(&pack_temp, (uint8_t*)temps);
    return EXIT_SUCCESS;
}

static int powerboard_update_pack_perc(void)
{
    // We can assume they were called in the proper order
    double last_pack_perc[PBD_LEN_PACK];
    double* last_pack_volt = dll_peek_head(&pack_volt.list);

    if(!last_pack_volt) {
        return EXIT_SUCCESS;
    }

    for(int i = 0; i < PBD_LEN_PACK; i++) {
        last_pack_perc[i] = (last_pack_volt[i] - 3.5) / (4.2 - 3.5);
    }

    log_add(&pack_perc, last_pack_perc);
    return EXIT_SUCCESS;
}

static int powerboard_update_pack_curr(void)
{
    uint8_t resp[2] = {0};
    ssize_t resp_len = sizeof(resp)/sizeof(resp[0]);

    if(acquire_serial_lock(serial_fd)) {
        P_ERR("Failed to acquire serial lock, errno: %d, (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    if(powerboard_send_read_bytes(serial_fd, 0x07, resp, &resp_len) == EXIT_FAILURE) {
        release_serial_lock(serial_fd);
        return EXIT_FAILURE;
    }
    
    if(release_serial_lock(serial_fd)) {
        P_ERR("Failed to release serial lock, errno: %d, (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    double result;
    uint16_t tmp = (uint16_t)((resp[0] << 8) | resp[1]);
    if (tmp >> 15 == 1) {
        tmp = (uint16_t)(~(tmp - 1));
        result = (double)(-1.0 * ((double)tmp * pbd_pack_curr_calib[0][0]) / pbd_pack_curr_calib[0][1]);
    } else {
        result = (double)(((double)tmp * pbd_pack_curr_calib[0][0]) / pbd_pack_curr_calib[0][1]);
    }

    log_add(&pack_curr, &result);
    return EXIT_SUCCESS;
}

static int powerboard_update_avg_perc(void)
{
    double* last_pack_perc = dll_peek_head(&pack_perc.list);

    if(!last_pack_perc) {
        return EXIT_FAILURE;
    }

    double last_avg_perc = 0;
    for(int i = 0; i < PBD_LEN_PACK; i++) {
        last_avg_perc += last_pack_perc[i];
    }

    last_avg_perc = last_avg_perc / PBD_LEN_PACK;

    log_add(&avg_perc, &last_avg_perc);
    return EXIT_SUCCESS;
}

static int powerboard_update_hs_curr(void)
{
    uint8_t resp[28] = {0};
    ssize_t resp_len = sizeof(resp)/sizeof(resp[0]);

    if(acquire_serial_lock(serial_fd)) {
        P_ERR("Failed to acquire serial lock, errno: %d, (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    if(powerboard_send_read_bytes(serial_fd, 0x0B, resp, &resp_len) == EXIT_FAILURE) {
        release_serial_lock(serial_fd);
        return EXIT_FAILURE;
    }
    
    if(release_serial_lock(serial_fd)) {
        P_ERR("Failed to release serial lock, errno: %d, (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    double last_hs_curr[PBD_LEN_HS];
    uint16_t* adc = (uint16_t*)resp;

    for(int i = 0; i < PBD_LEN_HS; i++) {
        adc[i] = be16toh(adc[i]);
        last_hs_curr[i] = (double)(pbd_hs_curr_calib[i][0] * adc[i] + pbd_hs_curr_calib[i][1]);
    }

    log_add(&hs_curr, last_hs_curr);
    return EXIT_SUCCESS;
}

static int powerboard_update_hs_flt(void)
{
    uint8_t resp[2] = {0};
    ssize_t resp_len = sizeof(resp)/sizeof(resp[0]);

    if(acquire_serial_lock(serial_fd)) {
        P_ERR("Failed to acquire serial lock, errno: %d, (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    if(powerboard_send_read_bytes(serial_fd, 0x0C, resp, &resp_len) == EXIT_FAILURE) {
        release_serial_lock(serial_fd);
        return EXIT_FAILURE;
    }
    
    if(release_serial_lock(serial_fd)) {
        P_ERR("Failed to release serial lock, errno: %d, (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    uint64_t last_hs_flt = be16toh(*((uint16_t*)resp));

    log_add(&hs_flt, &last_hs_flt);
    return EXIT_SUCCESS;
}

static time_t pbd_last_wd_tx = 0;

static int powerboard_update_watchdog(void)
{
    static const uint8_t wd_buf[4] = {'C', 'N', 'D', 'H'};
    time_t now = time(NULL);

    if(now - pbd_last_wd_tx > 10) {
        P_INFO_STR("Updating powerboard watchdog");
        pbd_last_wd_tx = now;

        if(acquire_serial_lock(serial_fd)) {
            P_ERR("Failed to acquire serial lock, errno: %d, (%s)", errno, strerror(errno));
            return EXIT_FAILURE;
        }

        ssize_t r = write(serial_fd, wd_buf, 4);
    
        if(release_serial_lock(serial_fd)) {
            P_ERR("Failed to release serial lock, errno: %d, (%s)", errno, strerror(errno));
            return EXIT_FAILURE;
        }

        if(r != 4) {
            P_ERR("Failed to update watchdog %zd, errno: %d (%s)", r, errno, strerror(errno));
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}

#ifdef SASSI_COMPAT
static powerboard_pib_t powerboard_pib;
#endif

int powerboard_init(void)
{
    if(start_serial(&serial_fd, PBD_SERIAL, B9600, 1, 0)) {
        P_ERR("Failed to setup %s", PBD_SERIAL);
        return EXIT_FAILURE;
    }

    const uint8_t log_mode = LOG_MODE_CSV;
    const uint64_t rate_limit_milli_s = 30 * 1000;

    log_init(&pack_volt, PBD_BIN, 1, LOG_FORMAT_DOUBLE, PBD_LEN_PACK, log_mode, 8, rate_limit_milli_s);
    log_init(&pack_perc, PBD_BIN, 2, LOG_FORMAT_DOUBLE, PBD_LEN_PACK, log_mode, 8, rate_limit_milli_s);
    log_init(&pack_temp, PBD_BIN, 3, LOG_FORMAT_FLOAT,  PBD_LEN_PACK, log_mode, 8, rate_limit_milli_s);
    log_init(&pack_curr, PBD_BIN, 4, LOG_FORMAT_DOUBLE, 1,            log_mode, 8, rate_limit_milli_s);
    log_init(&hs_curr,   PBD_BIN, 5, LOG_FORMAT_DOUBLE, PBD_LEN_HS,   log_mode, 8, rate_limit_milli_s);
    log_init(&hs_flt,    PBD_BIN, 6, LOG_FORMAT_U8,     4,            log_mode, 8, rate_limit_milli_s);
    log_init(&avg_perc,  PBD_BIN, 7, LOG_FORMAT_DOUBLE, 1,            log_mode, 8, rate_limit_milli_s);

#ifdef SASSI_COMPAT
    powerboard_pib_init(&powerboard_pib, "/dev/ttyUSB0", B115200);
#endif
    // Enable Watchdog
    uint8_t resp[6] = {0};
    ssize_t resp_len = sizeof(resp)/sizeof(resp[0]);

    if(acquire_serial_lock(serial_fd)) {
        P_ERR("Failed to acquire serial lock: errno: %d, (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    if(powerboard_send_read_bytes(serial_fd, 0x01, resp, &resp_len) == EXIT_FAILURE) {
        release_serial_lock(serial_fd);
        return EXIT_FAILURE;
    }

    if(release_serial_lock(serial_fd)) {
        P_ERR("Failed to release serial lock: errno: %d, (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

int powerboard_loop(void)
{
    powerboard_update_pack_volt();
    sleep(1);
    powerboard_update_watchdog();
    sleep(1);

    powerboard_update_pack_temp();
    sleep(1);
    powerboard_update_watchdog();
    sleep(1);

    powerboard_update_pack_perc();
    sleep(1);
    powerboard_update_watchdog();
    sleep(1);

    powerboard_update_pack_curr();
    sleep(1);

    powerboard_update_avg_perc();
    sleep(1);
    powerboard_update_watchdog();
    sleep(1);

    powerboard_update_hs_curr();
    sleep(1);
    powerboard_update_watchdog();
    sleep(1);

    powerboard_update_hs_flt();
    sleep(1);
    powerboard_update_watchdog();
    sleep(1);

    uint64_t cur_time = time(NULL);
    if(cur_time == log_time) {
        log_count++;
    } else {
        P_DEBUG("PBd refresh rate: %d", (int)log_count);
        log_count = 0;
        log_time = cur_time;
    }

    return EXIT_SUCCESS;
}

int powerboard_close(void)
{
    log_close(&pack_volt);
    log_close(&pack_perc);
    log_close(&pack_temp);
    log_close(&pack_curr);

    log_close(&hs_curr);
    log_close(&hs_flt);

    log_close(&avg_perc);

    close_serial(serial_fd);
    serial_fd = -1;

    return EXIT_SUCCESS;
}

// Not an updatable thing, but just commands
static int powerboard_hs_do(uint64_t mask, uint8_t byte)
{
    if(mask & 0xFFFF) { // Native hotswap
        uint8_t data[5] = {0xF0, 0x00, byte};
        
        uint16_t new_mask = htobe16((uint16_t)(mask & 0xFFFF));
        memcpy(data + 3, &new_mask, 2);

        P_INFO_STR("Attempting to set hotswap");
        
        if(acquire_serial_lock(serial_fd)) {
            P_ERR("Failed to acquire serial lock, errno: %d, (%s)", errno, strerror(errno));
            return EXIT_FAILURE;
        }

        ssize_t r = write(serial_fd, data, 5);

        if(r != 5) {
            P_ERR("Failed to set hotswap mask, %zd, errno: %d (%s)", r, errno, strerror(errno));
            release_serial_lock(serial_fd);
            return EXIT_FAILURE;
        }

        usleep(100*1000);

        uint8_t resp[5] = {0};
        ssize_t resp_len = sizeof(resp)/sizeof(resp[0]);

        if(powerboard_read_bytes(serial_fd, resp, &resp_len) == EXIT_FAILURE) {
            release_serial_lock(serial_fd);
            return EXIT_FAILURE;
        }
    
        if(release_serial_lock(serial_fd)) {
            P_ERR("Failed to release serial lock, errno: %d, (%s)", errno, strerror(errno));
            return EXIT_FAILURE;
        }
    }

#ifdef SASSI_COMPAT
    if(mask >> 16 && byte == 0x02) {
        usleep(100*1000); // allow PIB to boot?
        
        P_INFO_STR("Setting a PIB hotswap");
        
        if(powerboard_pib_enable(&powerboard_pib, (uint8_t)(mask >> 16))) {
            P_ERR_STR("Failed to set pib hotswaps");
            return EXIT_FAILURE;
        }
    } else if(mask >> 16 && byte == 0x03) {
        if(powerboard_pib_disable(%powerboard_pib, (uint8_t)(mask >> 16))) {
            P_ERR_STR("Failed to disable pib hotswaps");
            return EXIT_FAILURE;
        }
        // oh okay i'll recurse just this once
        // Either no hotswap or only 5V rail
        if(powerboard_pib.mask == 0 || powerboard_pib.mask == (HOTSWAP_PIB1_CH3 >> 16)) {
            // If we can't turn off the powerboard hotswap that's not a big deal
            powerboard_hs_dis(HOTSWAP_PIB1);
        }
    }
#endif

    return EXIT_SUCCESS;
}

int powerboard_hs_en(uint64_t mask)
{
    P_DEBUG("Enable mask: 0x%"PRIx64, mask);

    if(powerboard_hs_do(mask, 0x02)) {
        P_ERR("Failed to enable hotswap mask 0x%"PRIx64, mask);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

int powerboard_hs_dis(uint64_t mask)
{
    P_DEBUG("Disable mask: 0x%"PRIx64, mask);

    if(powerboard_hs_do(mask, 0x02)) {
        P_ERR("Failed to enable hotswap mask 0x%"PRIx64, mask);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

/*
int powerboard_htr_en(uint64_t mask){
    uint8_t data[5];
    data[0] = 0xF0;
    data[1] = 0x00;
    data[2] = 0x09;
    data[3] = mask >> 8;
    data[4] = mask & 0xFF;
    int r;
    if((r = writeSerial(serial_fd, data, 3))) {
        P_ERR("Failed to write serial to enable htr %d", r);
        return EXIT_FAILURE;
    }
    uint8_t resp[5] = {0};
    ssize_t resp_len = sizeof(resp)/sizeof(resp[0]);
    if(powerboard_read_bytes(serial_fd, resp, &resp_len) == EXIT_FAILURE) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

int powerboard_htr_dis(uint64_t mask){
    uint8_t data[5];
    data[0] = 0xF0;
    data[1] = 0x00;
    data[2] = 0x0A;
    data[3] = mask >> 8;
    data[4] = mask & 0xFF;
    int r;
    if((r = writeSerial(serial_fd, data, 3))) {
        P_ERR("Failed to write serial to disable htr %d", r);
        return EXIT_FAILURE;
    }
    uint8_t resp[5] = {0};
    ssize_t resp_len = sizeof(resp)/sizeof(resp[0]);
    if(powerboard_read_bytes(serial_fd, resp, &resp_len) == EXIT_FAILURE) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
*/

