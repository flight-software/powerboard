/**
 * Powerboard common utilities header file
 *
 * TODO: define a constant array for voltages based on hotswaps
 */
#ifndef _POWERBOARD_H_
#define _POWERBOARD_H_

#include <stdint.h>

// Binary
#define PBD_BIN "pb-d"

// UDP
#define PBD_RPC_SOCKET UINT16_C(50000)

// Serial
#define PBD_SERIAL "/dev/ttyO4"

// Battery Packs
#define PBD_OP_GET_PACK_VOLT 1
#define PBD_OP_GET_PACK_PERC 2
#define PBD_OP_GET_PACK_TEMP 3

// Hotswaps
#define PBD_OP_GET_HS_CURR 5
#define PBD_OP_GET_HS_FLT  6
#define PBD_OP_HS_EN       7
#define PBD_OP_HS_DIS      8

// Heater
//#define PBD_OP_HTR_EN 9
//#define PBD_OP_HTR_DIS 10

#define PBD_OP_GET_AVG_PERC 11

// Don't return raw ADC values

// Number of packs
#define PBD_LEN_PACK 4
#define PBD_ID_PACK_A_TOP 0
#define PBD_ID_PACK_A_BOT 1
#define PBD_ID_PACK_B_TOP 2
#define PBD_ID_PACK_B_BOT 3

// Offset
#define PBD_LEN_HS 14

// Native powerboard hotswaps
#define PBD_ID_HS_CH1_3V3_FLEX    0
#define PBD_ID_HS_CH2_3V3_CDH     1
#define PBD_ID_HS_CH3_3V3_UIUC    2
#define PBD_ID_HS_CH4_3V3_CD      3
#define PBD_ID_HS_CH5_3V3_SAIL    3
#define PBD_ID_HS_CH5_3V3_AM 	  4
#define PBD_ID_HS_CH6_3V3_VT 	  5
#define PBD_ID_HS_CH7_3V3_PYRO 	  6
#define PBD_ID_HS_CH8_7V4_FLEX    7
#define PBD_ID_HS_CH9_7V4_CDH     8
#define PBD_ID_HS_CH10_7V4_UIUC   9
#define PBD_ID_HS_CH11_7V4_CD     10
#define PBD_ID_HS_CH12_7V4_SAIL   10
#define PBD_ID_HS_CH12_7V4_AM     11
#define PBD_ID_HS_CH13_7V4_VTHEAT 12
#define PBD_ID_HS_CH14_7V4_VT     13

// Powerboard Native Hotswaps
#define HOTSWAP_CH0  0x0001
#define HOTSWAP_CH2  0x0002
#define HOTSWAP_CH3  0x0004
#define HOTSWAP_CH4  0x0008
#define HOTSWAP_CH5  0x0010
#define HOTSWAP_CH6  0x0020
#define HOTSWAP_CH7  0x0040
#define HOTSWAP_CH8  0x0080
#define HOTSWAP_CH9  0x0100
#define HOTSWAP_CH10 0x0200
#define HOTSWAP_CH11 0x0400
#define HOTSWAP_CH12 0x0800
#define HOTSWAP_CH13 0x1000
#define HOTSWAP_CH14 0x2000

// PIB hotswaps
#define HOTSWAP_PIB1 HOTSWAP_CH11     // always be on also
#define HOTSWAP_PIB1_CH1 (0x01 << 16) // Flame
#define HOTSWAP_PIB1_CH2 (0x02 << 16) // PSP
#define HOTSWAP_PIB1_CH3 (0x04 << 16) // 5V (always turn on)

#define HOTSWAP_J10  (HOTSWAP_CH11 | HOTSWAP_CH4)
#define HOTSWAP_J11  (HOTSWAP_CH12 | HOTSWAP_CH5)
#define HOTSWAP_ADCS (HOTSWAP_CH8  | HOTSWAP_CH0)

// Hotswap addresses (legacy, should not be used outside LAICE)
#define HOTSWAP_CH1_3V3FLEX 	HOTSWAP_CH0 // was CH1, is this correct?
#define HOTSWAP_CH2_3V3CDH 		HOTSWAP_CH2
#define HOTSWAP_CH3_3V3UIUC 	HOTSWAP_CH3
#define HOTSWAP_CH4_3V3CD 		HOTSWAP_CH4
#define HOTSWAP_CH5_3V3SAIL     HOTSWAP_CH4
#define HOTSWAP_CH5_3V3AM 		HOTSWAP_CH5
#define HOTSWAP_CH6_3V3VT 		HOTSWAP_CH6
#define HOTSWAP_CH7_PYRO 		HOTSWAP_CH7
#define HOTSWAP_CH8_7V4FLEX 	HOTSWAP_CH8
#define HOTSWAP_CH9_7V4CDH 		HOTSWAP_CH9
#define HOTSWAP_CH10_7V4UIUC 	HOTSWAP_CH10
#define HOTSWAP_CH11_7V4CD 		HOTSWAP_CH11
#define HOTSWAP_CH12_7V4SAIL    HOTSWAP_CH11
#define HOTSWAP_CH12_7V4AM 		HOTSWAP_CH12
#define HOTSWAP_CH13_7V4VTHEAT 	HOTSWAP_CH13
#define HOTSWAP_CH14_7V4VT 		HOTSWAP_CH14

#define PBD_OP_WAIT 5*1000*1000

#endif /* _POWERBOARD_H_ */
