#include <stdlib.h>

#include "powerboard_calib.h"

// doesn't need to use the header file, actually

// For now let's just assume it's linear
// 18650s range from 4.2 to around 2.8 being dead, and we have 7Ah per battery
// pack, then 3.5 * 7 == 24.5Wh of capacity

// NOTE: it would be really neat if we knew exactly the voltage where powerboard turned
// us off, and have this number reflect usable power

// Assuming all cells are balanced, we just multiply the capacity
static double powerboard_calib_generic_volt_watt_hour(double volt)
{
    const double slope = -24.5 / (4.2 - 2.8);
    const double intercept = -slope * 2.8;
    return slope * volt + intercept;
}

int powerboard_calib_volt_to_watt_hour(double* pack_volt, double* watt_hour)
{
    if(!watt_hour) return EXIT_FAILURE;

    *watt_hour = 0;
    for(size_t i = 0; i < PBD_LEN_PACK; i++) {
        *watt_hour += powerboard_calib_generic_volt_watt_hour(pack_volt[i]); 
    }

    return EXIT_SUCCESS;
}
