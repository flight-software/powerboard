#ifndef POWERBOARD_PIB_H
#define POWERBOARD_PIB_H

/**
 * \file powerboard_pib.h
 * \brief Powerboard PIB driver
 *
 * This is written for the PIB board command grammar
 *
 * Although the PIB is directly connected to a powerboard hotswap, nothing
 * should be directly enabling or disabling that. The PIB can be thought of as
 * a subnet for the devices it contains, so powering the hotswap depends only on
 * whether any of the hotswaps it contains are enabled
 *
 * The entire command grammar is one byte, and we just send it a bitmask of what
 * to enable, and it responds with an ACK/NACK bit and the hotswaps that are on
 *
 * You can have up to 3 PIBs if you really want to (beacuse we want to fit this
 * inside a u64 since that's a pretty round size)
 */

#include <stdint.h>

struct powerboard_pib_t
{
    int fd;
    uint8_t mask;
};

typedef struct powerboard_pib_t powerboard_pib_t;

int powerboard_pib_init(powerboard_pib_t* pib, const char* serial, speed_t speed);
void powerboard_pib_close(powerboard_pib_t* pib);

int powerboard_pib_enable(powerboard_pib_t* pib, uint8_t mask);
int powerboard_pib_disable(powerboard_pib_t* pib, uint8_t mask);
int powerboard_pib_set(powerboard_pib_t* pib, uint8_t mask);

#endif
