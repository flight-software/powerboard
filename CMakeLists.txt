cmake_minimum_required(VERSION 3.7)

if(TARGET pb-d)
    return()
endif()

project(pb-d)

include(ExternalProject)

set(CMAKE_C_STANDARD 99)
set(CMAKE_SYSTEM_NAME Linux)

set(DEBUG ON CACHE BOOL "Enable DEBUG build")
set(STRIP OFF CACHE BOOL "Strip binary")
set(CROSS_COMPILE OFF CACHE BOOL "Build for flight")
set(SASSI_COMPAT OFF CACHE BOOL "Enable PIB")
set(SPAWN_HMD ON CACHE BOOL "Enable HMD")
set(DAEMONIZE_PBD ON CACHE BOOL "PBD Should Daemonize")

function(set_compile_options target)
    target_compile_options(${target} PRIVATE
        -std=c99
        -Werror
        -Wfatal-errors
        -pedantic-errors
        -Wextra
        -Wall
        -Winit-self
        -Wmissing-include-dirs
        -Wswitch-default
        -Wswitch
        -Wfloat-equal
        -Wshadow
        -Wunsafe-loop-optimizations
        -Wcast-qual
        -Wconversion
        -Wlogical-op
        -Waggregate-return
        -Wstrict-prototypes
        -Wmissing-prototypes
        -Wmissing-declarations
        -Wnormalized=nfc
        -Wpacked
        -Wpadded
        -Wredundant-decls
        -Wnested-externs
        -Wunreachable-code
        -Wno-format
        -Wno-error=unused-function
        -Wno-error=sign-conversion
        -Wno-error=padded
        -Wno-error=unsafe-loop-optimizations
        -fno-strict-aliasing
    )
    
    # SASSI is special
    if(${SASSI_COMPAT})
        message("~~~~~~~~~BUILDING WITH SASSI_COMPAT ENABLED~~~~~~~~~~")
        target_compile_options(${target} PRIVATE
            -DSASSI_COMPAT
    )
    endif()

    if(${SPAWN_HMD})
        target_compile_options(${target} PRIVATE -DSPAWN_HMD)
    endif()

    if(${DAEMONIZE_PBD})
        target_compile_options(${target} PRIVATE -DDAEMONIZE_PBD)
    endif()

endfunction()

function(set_debug_options target debug cross)
    if(${debug})
        target_compile_options(${target} PRIVATE
            -Og
            -g
            -DDEBUG
        )
    else()
        target_compile_options(${target} PRIVATE
            -O3
            -Winline
            -DNDEBUG
        )
    endif()
endfunction()

set(FS_SRC_PBD_NAME pb-d)
set(FS_SRC_PBD_FILE pb-d)

set(FS_SRC_PBD_SRC ${PROJECT_SOURCE_DIR}/src/PBd.c
                   ${PROJECT_SOURCE_DIR}/src/powerboard_calib.c
                   ${PROJECT_SOURCE_DIR}/src/powerboard_hw.c
                   ${PROJECT_SOURCE_DIR}/src/powerboard_pib.c)

set(FS_SRC_PBD_HEADER ${PROJECT_SOURCE_DIR}/src/powerboard_calib.h
                      ${PROJECT_SOURCE_DIR}/src/powerboard_hw.h
                      ${PROJECT_SOURCE_DIR}/src/powerboard_pib.h)

add_executable(${FS_SRC_PBD_NAME} ${FS_SRC_PBD_SRC} ${FS_SRC_PBD_HEADER})

set_compile_options(${FS_SRC_PBD_NAME})

set_debug_options(${FS_SRC_PBD_NAME} ${DEBUG} ${CROSS_COMPILE})

target_include_directories(${FS_SRC_PBD_NAME} PUBLIC ${PROJECT_SOURCE_DIR}/src)

add_subdirectory(${PROJECT_SOURCE_DIR}/libdebug)
add_subdirectory(${PROJECT_SOURCE_DIR}/libdaemon)
add_subdirectory(${PROJECT_SOURCE_DIR}/librpc)
add_subdirectory(${PROJECT_SOURCE_DIR}/liblog)
add_subdirectory(${PROJECT_SOURCE_DIR}/libdll)
add_subdirectory(${PROJECT_SOURCE_DIR}/libtty)

add_dependencies(${FS_SRC_PBD_NAME} debug_lib)
add_dependencies(${FS_SRC_PBD_NAME} daemon)
add_dependencies(${FS_SRC_PBD_NAME} rpc)
add_dependencies(${FS_SRC_PBD_NAME} log)
add_dependencies(${FS_SRC_PBD_NAME} dll)
add_dependencies(${FS_SRC_PBD_NAME} tty)

target_link_libraries(${FS_SRC_PBD_NAME} PRIVATE debug_lib)
target_link_libraries(${FS_SRC_PBD_NAME} PRIVATE daemon)
target_link_libraries(${FS_SRC_PBD_NAME} PRIVATE rpc)
target_link_libraries(${FS_SRC_PBD_NAME} PRIVATE log)
target_link_libraries(${FS_SRC_PBD_NAME} PRIVATE dll)
target_link_libraries(${FS_SRC_PBD_NAME} PRIVATE tty)
target_link_libraries(${FS_SRC_PBD_NAME} PRIVATE m)

if(${CROSS_COMPILE})
    set(CMAKE_SYSTEM_PROCESSOR arm)
    set(CROSS_COMPILER_PREFIX "/usr")
    
    set(CROSS_COMPILER_BINDIR ${CROSS_COMPILER_PREFIX}/bin)
    set(CROSS_COMPILER_INCLUDE ${CROSS_COMPILER_PREFIX}/arm-linux-gnueabihf/usr/include)
    
    set(CMAKE_AR ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-ar CACHE FILEPATH "Archiver")
    set(CMAKE_CXX_COMPILER ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-g++)
    set(CMAKE_C_COMPILER ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-gcc)
    set(CMAKE_LINKER ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-ld)
    set(CMAKE_NM ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-nm)
    set(CMAKE_OBJCOPY ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-objcopy)
    set(CMAKE_OBJDUMP ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-objdump)
    set(CMAKE_RANLIB ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-ranlib CACHE FILEPATH "Runlib")
    set(CMAKE_STRIP ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-strip)

    set(CMAKE_FIND_ROOT_PATH ${CROSS_COMPILER_PREFIX}/arm-linux-gnueabihf)

    set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
    set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
    set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
    set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

    if(${STRIP})
        add_custom_command(TARGET ${FS_SRC_PBD_NAME}
            POST_BUILD
            COMMAND ${CMAKE_STRIP} --strip-all ${FS_SRC_PBD_FILE}
        )
    endif()
else()
    if(${STRIP})
        add_custom_command(TARGET ${FS_SRC_PBD_NAME}
            POST_BUILD
            COMMAND strip --strip-all ${FS_SRC_PBD_FILE}
        )
    endif()
endif()
